#!make

# Get variable from environment
include .env
export

# Work with such make commands, but not with file with the same name
.PHONY: ansible

ansible:
	docker build ansible --tag ansible-service:latest
	docker run --rm -it \
		 --env-file .env \
		 ansible-service:latest \
		 ${cmd}

deploy:
	docker-compose -f ./ansible/docker-compose.yaml up --build ansible

build:
	docker build . -t ${DOCKER_USERNAME}/${APP_NAME}:${APP_TAG}
	docker push ${DOCKER_USERNAME}/${APP_NAME}:${APP_TAG}
