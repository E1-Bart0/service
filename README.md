# Service

## Runtime
Приложение отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Как работать с приложением в docker:  
1. Установить docker. Скопировать переменнные окружения:
   ```shell
   cp .env.template .env
   ```
2. Запустить тесты
   ```shell
   ./run-tests.sh
   ```
3. Собрать:
   ```shell
   docker-compose build
   ```
   or
   ```shell
   docker build . -t skillbox/app
   ```
4. Запустить:
   ```shell
   docker-compose up
   ```
   or
   ```shell
   docker run -p 8080:8080 skillbox/app
   ```

## Deploy Service via Docker Hub and Ansible

### REQUIRED

- AWS tokens in *.env* _(For dynamic inventory file)_.
- Private key in _./ansible/keys/id_rsa_ _(Connect to servers via SSH)_

### To create repository in Docker Hub

Specify your **DOCKER_USERNAME** in .env. Also, you can change **APP_NAME**, **APP_TAG**.

### To build app and push it into the repository:

```shell
make build
```
It will build image with a name: **DOCKER_USERNAME / APP_NAME** and tag: **APP_TAG**. 
And push that image to the repository: **DOCKER_USERNAME / APP_NAME**. 

Taking variables from _.env_.

### To deploy app via Ansible

```shell
make deploy
```

Using your image in Docker Hub Ansible will start docker container in systemctl. More info in role: _deploy_unit_file_
### There are some ways to use ansible:

#### Via docker:

You can just type: **make ansible cmd="_Your command_".** For example
```
make ansible cmd="ansible --version"
make ansible cmd="ansible-playbook -b ./playbooks/install_app.yaml"    
```

#### Via docker-compose

```
cd ansible
docker-compose up ansible
```
