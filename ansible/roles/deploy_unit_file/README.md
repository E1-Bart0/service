# Deploy unit file
=========

This role will create a system file (unit-file) and launch it

Requirements
------------
Docker should be installed at the host

Role Variables
--------------

Variables: 
    
- app:
   * name: Name of the application or docker repository. **REQUIRED** to download the image
   * user: DockerUsername. **REQUIRED** to download the image
   * tag: Image tag. **REQUIRED** to download the image
   * internal_port: Internal app port. **REQUIRED** 
   * external_port: External app port. **REQUIRED**

- service:
  * name: Name of the service
  * description: Description of the service

- template_name: What template do you want to use
- remote_destination: Path to store the unit file


- template:
  * app:
    * description: Description of the service _(Taking from service.description)_

  * unit:
    * requires: This directive lists any units upon which this unit essentially depends
    * after: The units listed in this directive will be started before starting the current unit

  service:
    user: User
    group: Group
    timeout_start_sec: his configures the amount of time that systemd will wait when starting the service
    restart: This indicates the circumstances under which systemd will attempt to automatically restart the service
    pre_start: This can be used to provide additional commands that should be executed before the main process is started
    start: This specifies the full path and the arguments of the command to be executed to start the process
    reload: This optional directive indicates the command necessary to reload the configuration of the service if available
    stop: This indicates the command needed to stop the service
    timeout_sec: This configures the amount of time that systemd will wait when stopping or stopping the service before marking it as failed or forcefully killing it
    oom_score_adjust: Sets the adjustment level for the Out-Of-Memory killer for executed processes. Takes an integer between -1000 and 1000 

  install:
    wanted_by: Is the most common way to specify how a unit should be enabled.


Dependencies
------------

No 

Example Playbook
----------------


```
---
- hosts: _true
  become: true
  vars:
    app:
      name: "{{ lookup('env', 'APP_NAME') | default('service-web', True) }}"
      user: "{{ lookup('env', 'DOCKER_USERNAME') | default('e1bart0', True) }}"
      tag: "{{ lookup('env', 'APP_TAG') | default('latest', True) }}"
      internal_port: "{{ lookup('env', 'APP_INTERNAL_PORT') | default('8080', True) }}"
      external_port: "{{ lookup('env', 'APP_EXTERNAL_PORT') | default('80', True) }}"
    service:
      name: "{{ app.name }}.service"
      description: "Skillbox Web Service"

  pre_tasks:
    - name: Docker Demon is started
      service:
        name: docker
        state: started

  roles:
    - role: deploy_unit_file

```
